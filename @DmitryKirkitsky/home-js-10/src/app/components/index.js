import $ from 'jquery'


let allData =[];
let usersData=[];

let url = 'https://reqres.in/api/users';

function getData(method, myUrl){
  return fetch(url).then(res =>{
    return res.json();
  })
}
getData('GET', url).then(data => allData.push(data));
console.log(allData);
show(allData);

function show(data1){
  for (let i=0; i<data1.length;i++){
    let iterator = data1[i];
    console.log(iterator);
    for (let k=0; k<iterator.data.length;k++){
      let it = iterator.data[k];
      console.log(it.id);
      $('.usersList').append(`
      <div class="usersList__item">
        <div class="row pad-10">
          <div class="col-6">
            <div class="usersList__item--img" data-img="${it.avatar}">
              <img src="${it.avatar}">
            </div>
          </div>
          <div class="col-6">
            <div class="usersList__item--description" data-name="${it.first_name, it.last_name}">
              <p>Name: ${it.first_name} ${it.last_name}</p>
              <button type="button" id="userFullInfo" class="usersList__item--description--btn" data-userindex = "${it.id}">See more details</button>
            </div>
          </div>
        </div>
      </div>
      `);
    }
  }
}
