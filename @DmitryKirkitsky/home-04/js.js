const button = document.getElementsByClassName('btn-openModal')[0];
const closebutton = document.getElementsByClassName('btn-close')[0];
const savebutton = document.getElementsByClassName('btn-save')[0];
const okbutton = document.getElementsByClassName('btn-Ok')[0];
const crossbutton = document.getElementsByClassName('btn-cross')[0];
const modal = document.getElementById('OpenModal');
const modal1 = document.getElementById('SaveModal');
button.onclick = () => {
  toggle(true);
}
closebutton.onclick = () => {
  toggle(false);
}
savebutton.onclick = () => {
  toggle1(true);
}
okbutton.onclick = () => {
  toggle1(false);
}
crossbutton.onclick = () => {
  toggle(false);
}
modal.onclick = (e) =>{
  toggle(false,e);
}
modal1.onclick = (e) =>{
  toggle1(false,e);
}
function toggle(isBlock,e){
  if (e && e.target.id !=="OpenModal"){
      return
  }
  modal.style.display = isBlock ? 'block' : 'none'
}
function toggle1(isBlock,e){
  if (e && e.target.id !=="SaveModal"){
      return
  }
  modal1.style.display = isBlock ? 'block' : 'none'
}
