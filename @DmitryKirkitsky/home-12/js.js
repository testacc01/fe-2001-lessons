const button = document.getElementsByClassName('header__burgerButton--OpenButton')[0];
const button1 = document.getElementsByClassName('header__burgerButton--CloseButton')[0];
const div = document.getElementsByClassName('header__OpenBurgerButton')[0];
const modal = document.getElementById('Header__burger');
button.onclick = () => {
  toggle(true);
  div.style.display = 'none';
  button1.style.display = 'block';
}
button1.onclick = () => {
  toggle(false);
  div.style.display = 'block';
  button1.style.display = 'none';
}
modal.onclick = (e) =>{
  toggle(false,e);
}
function toggle(isBlock,e){
  if (e && e.target.id !=="Header__burger"){
      return
  }
  modal.style.display = isBlock ? 'block' : 'none'
}
