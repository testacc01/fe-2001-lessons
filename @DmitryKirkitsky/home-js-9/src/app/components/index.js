import $ from 'jquery'

let data = [
  {
    "_id": "5e9abd32b830663252a1a553",
    "index": 0,
    "guid": "cd2cd136-9664-4757-823e-412a0581626e",
    "isActive": false,
    "balance": "$1,405.94",
    "picture": "http://placehold.it/32x32",
    "age": 35,
    "eyeColor": "blue",
    "name": {
      "first": "Nolan",
      "last": "Dodson"
    },
    "company": "GENMOM",
    "email": "nolan.dodson@genmom.us",
    "phone": "+1 (809) 410-2587",
    "address": "594 Lyme Avenue, Sutton, Minnesota, 6751",
    "about": "Non nostrud exercitation cillum cupidatat officia consequat anim id excepteur non velit anim. Aliquip qui occaecat ad dolore non adipisicing nostrud culpa. Elit est non esse ipsum occaecat nulla cillum enim ex. Aliqua id tempor dolor aute ullamco consequat cupidatat incididunt duis labore officia adipisicing. Pariatur consequat ex reprehenderit irure aliqua consectetur.",
    "registered": "Thursday, July 16, 2015 1:23 PM",
    "friends": [
      {
        "id": 0,
        "name": "Liz Underwood"
      },
      {
        "id": 1,
        "name": "Jennie Lynn"
      },
      {
        "id": 2,
        "name": "Kristine Cochran"
      }
    ],
    "greeting": "Hello, Nolan! You have 10 unread messages.",
    "favoriteFruit": "apple"
  },
  {
    "_id": "5e9abd3265bc71d749924ace",
    "index": 1,
    "guid": "bc7ea661-cebf-4cb6-b13c-70c511ce37f8",
    "isActive": true,
    "balance": "$1,378.85",
    "picture": "http://placehold.it/32x32",
    "age": 25,
    "eyeColor": "brown",
    "name": {
      "first": "Castillo",
      "last": "Berg"
    },
    "company": "QUILTIGEN",
    "email": "castillo.berg@quiltigen.net",
    "phone": "+1 (876) 564-2364",
    "address": "224 Kenilworth Place, Spelter, Arizona, 6558",
    "about": "Sit in duis deserunt reprehenderit eiusmod non minim dolor velit veniam. Duis ut irure esse id amet culpa aliquip amet incididunt ipsum excepteur sint aliquip. Dolore quis Lorem fugiat duis. Tempor commodo ex irure aliqua reprehenderit ipsum. Consequat ipsum nostrud dolor aliqua id nostrud deserunt magna adipisicing deserunt. Pariatur laboris enim occaecat culpa ullamco excepteur sunt ipsum dolore anim amet anim. Enim pariatur aliqua duis Lorem qui et commodo adipisicing.",
    "registered": "Sunday, December 20, 2015 4:36 AM",
    "friends": [
      {
        "id": 0,
        "name": "Oliver Price"
      },
      {
        "id": 1,
        "name": "Ramirez Justice"
      },
      {
        "id": 2,
        "name": "Brooks Wood"
      }
    ],
    "greeting": "Hello, Castillo! You have 8 unread messages.",
    "favoriteFruit": "strawberry"
  },
  {
    "_id": "5e9abd32a899a5f7637128cf",
    "index": 2,
    "guid": "a673fa9b-283d-40a8-a9a7-67fd98b93f3b",
    "isActive": true,
    "balance": "$3,211.36",
    "picture": "http://placehold.it/32x32",
    "age": 40,
    "eyeColor": "green",
    "name": {
      "first": "Agnes",
      "last": "Beck"
    },
    "company": "PARLEYNET",
    "email": "agnes.beck@parleynet.co.uk",
    "phone": "+1 (856) 507-3029",
    "address": "237 Channel Avenue, Warsaw, Delaware, 903",
    "about": "Id excepteur voluptate commodo labore cupidatat et cupidatat ea velit nostrud. Ea nisi non laboris aliquip consequat commodo non anim officia deserunt occaecat labore occaecat. Pariatur nostrud ut culpa anim qui occaecat.",
    "registered": "Thursday, February 12, 2015 6:36 PM",
    "friends": [
      {
        "id": 0,
        "name": "Ford Sosa"
      },
      {
        "id": 1,
        "name": "Gabrielle Heath"
      },
      {
        "id": 2,
        "name": "Bryant Colon"
      }
    ],
    "greeting": "Hello, Agnes! You have 5 unread messages.",
    "favoriteFruit": "apple"
  },
  {
    "_id": "5e9abd32e07ba54a8f034819",
    "index": 3,
    "guid": "fc5c988c-259b-45f4-8923-91ac986a632d",
    "isActive": false,
    "balance": "$2,524.56",
    "picture": "http://placehold.it/32x32",
    "age": 29,
    "eyeColor": "brown",
    "name": {
      "first": "Carver",
      "last": "Rutledge"
    },
    "company": "WEBIOTIC",
    "email": "carver.rutledge@webiotic.org",
    "phone": "+1 (968) 506-3048",
    "address": "239 Hunterfly Place, Lloyd, Federated States Of Micronesia, 6019",
    "about": "Laborum minim labore cillum deserunt officia cillum esse. Laboris eiusmod velit id non duis ullamco id cupidatat. Laborum velit mollit ad labore consectetur incididunt. In adipisicing exercitation velit in. Velit duis nostrud voluptate culpa commodo eiusmod Lorem anim.",
    "registered": "Tuesday, April 12, 2016 7:48 AM",
    "friends": [
      {
        "id": 0,
        "name": "Wolfe Mccoy"
      },
      {
        "id": 1,
        "name": "Holden Hull"
      },
      {
        "id": 2,
        "name": "Janine Durham"
      }
    ],
    "greeting": "Hello, Carver! You have 7 unread messages.",
    "favoriteFruit": "banana"
  },
  {
    "_id": "5e9abd321b1d9075a80a2e88",
    "index": 4,
    "guid": "f446ff8f-aa63-4a61-ad7a-e02fd17435eb",
    "isActive": false,
    "balance": "$3,775.05",
    "picture": "http://placehold.it/32x32",
    "age": 38,
    "eyeColor": "brown",
    "name": {
      "first": "Ingram",
      "last": "Stevenson"
    },
    "company": "JETSILK",
    "email": "ingram.stevenson@jetsilk.ca",
    "phone": "+1 (921) 576-2744",
    "address": "223 Bath Avenue, Hoehne, Pennsylvania, 4351",
    "about": "Aliquip in aliqua commodo laboris. Eu ex ut incididunt duis voluptate sit adipisicing. Duis cillum aute ullamco consectetur. Laborum sit ad laboris exercitation.",
    "registered": "Thursday, January 16, 2014 9:01 AM",
    "friends": [
      {
        "id": 0,
        "name": "Daniel Bowman"
      },
      {
        "id": 1,
        "name": "Higgins Abbott"
      },
      {
        "id": 2,
        "name": "Willa Gardner"
      }
    ],
    "greeting": "Hello, Ingram! You have 8 unread messages.",
    "favoriteFruit": "apple"
  }
];

let allNames = [];
let allEyeColors = [];
let allAges = [];
let allFavoriteFruit = [];
let allSalarys = [];



function constructorPerson(list, personHolder, filterHolder, modal){
  this.list = list;
  this.personHolder = personHolder;
  this.filterHolder = filterHolder;
  this.modal = modal;
  this.nameSearch = '';
  this.eyeColor = '';
  this.agefrom = '';
  this.ageto = '';
  this.rangeBalance = '';


  this.init = function(){
    this.initPersonList(this.list);
    this.initPersonFilters();
    this.addEventsListeners();
  }

  this.initPersonList = function(list){
    const self = this;
    this.personHolder.html('');
    list.map(function(elem,index){
      self.personHolder.append(`
      <div class="usersList__item">
        <div class="row pad-10">
          <div class="col-6">
            <div class="usersList__item--img">
              <img src="images/avatar.jpg">
            </div>
          </div>
          <div class="col-6">
            <div class="usersList__item--description" data-name="${elem.name.first, elem.name.last}"
              data-age="${elem.age}" data-email="${elem.email}"
              data-phone="${elem.phone}" data-userindex="${elem.index}">
              <p>Name: ${elem.name.first} ${elem.name.last}</p>
              <p>Age: ${elem.age}</p>
              <p>Email:<a href="mailto:${elem.email}"> ${elem.email}</a></p>
              <p>Phone:<a href="tel:${elem.phone}"> ${elem.phone}</a></p>
              <button type="button" id="userFullInfo" class="usersList__item--description--btn" data-userindex = "${elem.index}">See more details</button>
            </div>
          </div>
        </div>
      </div>
      `);
    })

    for (let i = 0; i < data.length; i++){
      let iterator = data[i];
      allNames.indexOf(iterator.name.first, iterator.name.last) == -1 ? allNames.push(iterator.name.first+' '+iterator.name.last) : null;
      allEyeColors.indexOf(iterator.eyeColor) == -1 ? allEyeColors.push(iterator.eyeColor) : null;
      allAges.indexOf(iterator.age) == -1 ? allAges.push(iterator.age) : null;
      allFavoriteFruit.indexOf(iterator.favoriteFruit) == -1 ? allFavoriteFruit.push(iterator.favoriteFruit) : null;
      allSalarys.indexOf(iterator.balance) == -1 ? allSalarys.push(iterator.balance) : null;}

  }

  this.initPersonFilters = function(){
    this.showAllFilters();
  }
  this.showAllFilters = function(){
    $('.usersFilter').append(`
        <p>Filters</p>
        <p>Enter name</p>
        <input type="text" id="nameSearch">
        <p>Choose eye color</p>`);

      for (let i=0; i<allEyeColors.length;i++){
        $('.usersFilter').append(`<input type="radio" id="${allEyeColors[i]}" name="eyeColor" value="${allEyeColors[i]}">
        <label for="${allEyeColors[i]}">${allEyeColors[i]}</label>`)
      };

      $('.usersFilter').append(`<p>Enter age</p>
      <label for="agefrom">from</label>
      <input type="text" id="agefrom">
      <label for="ageto">to</label>
      <input type="text" id="ageto">
      <p>Choose favorite fruit</p>`);

      for (let i=0; i<allFavoriteFruit.length;i++){
        $('.usersFilter').append(`<input type="checkbox" id="${allFavoriteFruit[i]}" name="fruit" value="allFavoriteFruit[i]">
        <label for="${allFavoriteFruit[i]}">${allFavoriteFruit[i]}</label>`)
      };

      $('.usersFilter').append(`<p>Choose the salary</p>
      <input type="range" id="range">
      <button type="button" id="filter">Filter</button>
    `);
  }

  this.addEventsListeners = function(){
    const self = this;

    $('.usersList__item--description--btn').on('click', function(){

        let currentEl = event.target;
        $('.modal').addClass('show');
        $('.modal__Header').html(`<p>Full Information About <span class="modal__Header--Name">${self.list[currentEl.dataset.userindex].name.first} ${self.list[currentEl.dataset.userindex].name.last}</span></p>`);
        $('.modal__Info').html(`
        <p><span class="modal__Header--Name">Balance: </span>${self.list[currentEl.dataset.userindex].balance}</p>
        <p><span class="modal__Header--Name">Eye color: </span>${self.list[currentEl.dataset.userindex].eyeColor}</p>
        <p><span class="modal__Header--Name">Company: </span>${self.list[currentEl.dataset.userindex].company}</p>
        <p><span class="modal__Header--Name">Address: </span>${self.list[currentEl.dataset.userindex].address}</p>
        <p class="aboutMe"><span class="modal__Header--Name">About me: </span>${self.list[currentEl.dataset.userindex].about}</p>
        <p><span class="modal__Header--Name">Favorite friut: </span>${self.list[currentEl.dataset.userindex].favoriteFruit}</p>
        `);
        $('.modal__Close').html(`<span class="modal__Close--closebtn">X</span>`);
        $('.modal__Close--closebtn').on('click', function(){
          $('.modal').removeClass('show');
        })
    });

    $('#filter').on('click', function(){
      self.filtering();
    })

    $('#nameSearch').on('change', function(e){
      self.nameSearch = e.target.value;
    })

    $('#agefrom').on('change', function(e){
      self.agefrom = e.target.value;
    })

    $('#ageto').on('change', function(e){
      self.ageto = e.target.value;
    })

    $('#range').on('change', function(e){
      self.rangeBalance = e.target.value;
    })

  }

  this.filtering = function(){
    const self = this;
    let newArray = this.list.filter(function(item){
      return item.name.first.indexOf(self.nameSearch) > -1
              && (self.agefrom === '' || self.agefrom <= item.age)
              && (self.ageto === '' || self.ageto >= item.age)
              && (self.rangeBalance === '' || self.rangeBalance == item.balance)
    });
    this.initPersonList(newArray);
    console.log(newArray);
  }

}

let personList = new constructorPerson(data,$('.usersList'), $('usersFilter'), $('.modal'))
personList.init();
