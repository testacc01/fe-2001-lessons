var modal = document.getElementsByClassName("nav__burger")[0];
var btn1 = document.getElementsByClassName("open__burger")[0];
var btn2 = document.getElementsByClassName("nav__burger--close")[0];


btn1.onclick = function() {
    modal.style.display = "block";
    btn1.style.display = "none";
}

btn2.onclick = function() {
    modal.style.display = "none";
    btn1.style.display = "block";
}

window.onclick = function(event) {
    if (event.target == modal) {
        modal.style.display = "none";
        btn1.style.display = "block";    
    }
}
