import $ from 'jquery';

var userList = [
    {
      "_id": "5e9abd32b830663252a1a553",
      "index": 0,
      "guid": "cd2cd136-9664-4757-823e-412a0581626e",
      "isActive": false,
      "balance": "$1,405.94",
      "picture": "./img/lesson-28/1.jpg",
      "age": 35,
      "eyeColor": "blue",
      "name": {
        "first": "Nolan",
        "last": "Dodson"
      },
      "company": "GENMOM",
      "email": "nolan.dodson@genmom.us",
      "phone": "+1 (809) 410-2587",
      "address": "594 Lyme Avenue, Sutton, Minnesota, 6751",
      "about": "Non nostrud exercitation cillum cupidatat officia consequat anim id excepteur non velit anim. Aliquip qui occaecat ad dolore non adipisicing nostrud culpa. Elit est non esse ipsum occaecat nulla cillum enim ex. Aliqua id tempor dolor aute ullamco consequat cupidatat incididunt duis labore officia adipisicing. Pariatur consequat ex reprehenderit irure aliqua consectetur.",
      "registered": "Thursday, July 16, 2015 1:23 PM",
      "friends": [
        {
          "id": 0,
          "name": "Liz Underwood"
        },
        {
          "id": 1,
          "name": "Jennie Lynn"
        },
        {
          "id": 2,
          "name": "Kristine Cochran"
        }
      ],
      "greeting": "Hello, Nolan! You have 10 unread messages.",
      "favoriteFruit": "apple"
    },
    {
      "_id": "5e9abd3265bc71d749924ace",
      "index": 1,
      "guid": "bc7ea661-cebf-4cb6-b13c-70c511ce37f8",
      "isActive": true,
      "balance": "$1,378.85",
      "picture": "./img/lesson-28/2.jpg",
      "age": 25,
      "eyeColor": "brown",
      "name": {
        "first": "Castillo",
        "last": "Berg"
      },
      "company": "QUILTIGEN",
      "email": "castillo.berg@quiltigen.net",
      "phone": "+1 (876) 564-2364",
      "address": "224 Kenilworth Place, Spelter, Arizona, 6558",
      "about": "Sit in duis deserunt reprehenderit eiusmod non minim dolor velit veniam. Duis ut irure esse id amet culpa aliquip amet incididunt ipsum excepteur sint aliquip. Dolore quis Lorem fugiat duis. Tempor commodo ex irure aliqua reprehenderit ipsum. Consequat ipsum nostrud dolor aliqua id nostrud deserunt magna adipisicing deserunt. Pariatur laboris enim occaecat culpa ullamco excepteur sunt ipsum dolore anim amet anim. Enim pariatur aliqua duis Lorem qui et commodo adipisicing.",
      "registered": "Sunday, December 20, 2015 4:36 AM",
      "friends": [
        {
          "id": 0,
          "name": "Oliver Price"
        },
        {
          "id": 1,
          "name": "Ramirez Justice"
        },
        {
          "id": 2,
          "name": "Brooks Wood"
        }
      ],
      "greeting": "Hello, Castillo! You have 8 unread messages.",
      "favoriteFruit": "strawberry"
    },
    {
      "_id": "5e9abd32a899a5f7637128cf",
      "index": 2,
      "guid": "a673fa9b-283d-40a8-a9a7-67fd98b93f3b",
      "isActive": true,
      "balance": "$3,211.36",
      "picture": "./img/lesson-28/3.jpg",
      "age": 40,
      "eyeColor": "green",
      "name": {
        "first": "Agnes",
        "last": "Beck"
      },
      "company": "PARLEYNET",
      "email": "agnes.beck@parleynet.co.uk",
      "phone": "+1 (856) 507-3029",
      "address": "237 Channel Avenue, Warsaw, Delaware, 903",
      "about": "Id excepteur voluptate commodo labore cupidatat et cupidatat ea velit nostrud. Ea nisi non laboris aliquip consequat commodo non anim officia deserunt occaecat labore occaecat. Pariatur nostrud ut culpa anim qui occaecat.",
      "registered": "Thursday, February 12, 2015 6:36 PM",
      "friends": [
        {
          "id": 0,
          "name": "Ford Sosa"
        },
        {
          "id": 1,
          "name": "Gabrielle Heath"
        },
        {
          "id": 2,
          "name": "Bryant Colon"
        }
      ],
      "greeting": "Hello, Agnes! You have 5 unread messages.",
      "favoriteFruit": "apple"
    },
    {
      "_id": "5e9abd32e07ba54a8f034819",
      "index": 3,
      "guid": "fc5c988c-259b-45f4-8923-91ac986a632d",
      "isActive": false,
      "balance": "$2,524.56",
      "picture": "./img/lesson-28/4.jpeg",
      "age": 29,
      "eyeColor": "brown",
      "name": {
        "first": "Carver",
        "last": "Rutledge"
      },
      "company": "WEBIOTIC",
      "email": "carver.rutledge@webiotic.org",
      "phone": "+1 (968) 506-3048",
      "address": "239 Hunterfly Place, Lloyd, Federated States Of Micronesia, 6019",
      "about": "Laborum minim labore cillum deserunt officia cillum esse. Laboris eiusmod velit id non duis ullamco id cupidatat. Laborum velit mollit ad labore consectetur incididunt. In adipisicing exercitation velit in. Velit duis nostrud voluptate culpa commodo eiusmod Lorem anim.",
      "registered": "Tuesday, April 12, 2016 7:48 AM",
      "friends": [
        {
          "id": 0,
          "name": "Wolfe Mccoy"
        },
        {
          "id": 1,
          "name": "Holden Hull"
        },
        {
          "id": 2,
          "name": "Janine Durham"
        }
      ],
      "greeting": "Hello, Carver! You have 7 unread messages.",
      "favoriteFruit": "banana"
    },
    {
      "_id": "5e9abd321b1d9075a80a2e88",
      "index": 4,
      "guid": "f446ff8f-aa63-4a61-ad7a-e02fd17435eb",
      "isActive": false,
      "balance": "$3,775.05",
      "picture": "./img/lesson-28/5.jpg",
      "age": 38,
      "eyeColor": "brown",
      "name": {
        "first": "Ingram",
        "last": "Stevenson"
      },
      "company": "JETSILK",
      "email": "ingram.stevenson@jetsilk.ca",
      "phone": "+1 (921) 576-2744",
      "address": "223 Bath Avenue, Hoehne, Pennsylvania, 4351",
      "about": "Aliquip in aliqua commodo laboris. Eu ex ut incididunt duis voluptate sit adipisicing. Duis cillum aute ullamco consectetur. Laborum sit ad laboris exercitation.",
      "registered": "Thursday, January 16, 2014 9:01 AM",
      "friends": [
        {
          "id": 0,
          "name": "Daniel Bowman"
        },
        {
          "id": 1,
          "name": "Higgins Abbott"
        },
        {
          "id": 2,
          "name": "Willa Gardner"
        }
      ],
      "greeting": "Hello, Ingram! You have 8 unread messages.",
      "favoriteFruit": "apple"
    }
  ]

// var usersList = [
//   {
//     "id": 1,
//     "email": "nolan.dodson@genmom.us",
//     "first_name": "Nolan",
//     "last_name": "Dodson",
//     "avatar": "./img/lesson-28/1.jpg",
//     "isMale": true,
//     "city": "Minsk",
//     "job": "IT",
//     "age": 30
//   },
//    {
//     "id": 2,
//     "email": "Balan@genmom.us",
//     "first_name": "Dan",
//     "last_name": "Balan",
//     "avatar": "./img/lesson-28/2.jpg",
//     "isMale": true,
//     "city": "Barcelona",
//     "job": "IT",
//     "age": 35
//   },
//    {
//     "id": 3,
//     "email": "Belucchi@genmom.us",
//     "first_name": "Monica",
//     "last_name": "Belucchi",
//     "avatar": "./img/lesson-28/3.jpg",
//     "isMale": false,
//     "city": "Paris",
//     "job": "Aktors",
//     "age": 40
//   },
//    {
//     "id": 4,
//     "email": "Motya@genmom.us",
//     "first_name": "Tetya",
//     "last_name": "Motya",
//     "avatar": "./img/lesson-28/4.jpeg",
//     "isMale": false,
//     "city": "Moskow",
//     "job": "Musiciant",
//     "age": 25
//   },
//    {
//     "id": 5,
//     "email": "Willis@genmom.us",
//     "first_name": "Bruce",
//     "last_name": "Willis",
//     "avatar": "./img/lesson-28/5.jpg",
//     "isMale": true,
//     "city": "Borisov",
//     "job": "Dance",
//     "age": 20
//   }
// ]


// User list & User filter
function ConstructorPerson(list, personsHolder, filtersHolder) {
  this.list = list;
  this.personsHolder = personsHolder;
  this.filtersHolder = filtersHolder;

  this.nameSearch = '';
  this.companyList = '';
  //this.age = ['', ''];
  this.male = '';

  this.init = function() {
    this.initPersonsList(this.list);
    this.initPersonsFilters();
    this.addEventsListeners();
  }
  this.initPersonsList = function(list) {
    const self = this;
    this.personsHolder.html('');
    list.map(function(elem, index) {
      self.personsHolder.append(`
      <div class="persons__list--item">
        <img src="${elem.picture}">
        <p>${elem.name.first} ${elem.name.last}</p>
       <button class="show__more" data-id = '${elem._id}'>Show full info</button>
      </div>`)
    })
  }
  this.initPersonsFilters = function() {
    var uniqCompany = [];
    for(var i = 0; i < this.list.length; i++) {
      uniqCompany.indexOf(this.list[i].company) == -1 ? uniqCompany.push(this.list[i].company) : null;
    }
    uniqCompany.map(function(elem, index) {
      $("#companyList").append(`<option value = "${elem}">${elem}</option>`)
    })
  } 
  this.addEventsListeners = function() {
    const self = this;
    $("#submit").on("click", function() {
      self.filtering();
    })
    $("#nameSearch").on("change", function(e) {
      self.nameSearch = e.target.value;
    })
    $("#male").on("change", function(e) {
      self.male = e.target.checked;
    })
    $("#jobsList").on("change", function(e) {
      self.jobsList = $(this).children("option:selected").val();
    })
  }
  this.filtering = function() {
    const self = this;
    var newArr = this.list.filter(function(item) {
      return item.first_name.indexOf(self.nameSearch) > -1 && (self.jobsList === "" || self.jobsList == item.job) && (self.male === "" || self.male == item.isMale)
    })
    this.initPersonsList(newArr);
  }
}

let personListForm = new ConstructorPerson(usersList, $(".persons__list"), $(".persons__filters"));
personListForm.init();

// Button “Show full info”
function personInfo() {
  $('.show__more').each(function(i, element){
    element.onclick = openModal;
  })
   
  $("#closeMyModal").on("click", function(){   
        $("#myModal").css("display", "none");
        $("#modal-text--item").empty();
    });
}

function openModal(event) {
  var personsclick = event.target.dataset.userId;
  $("#myModal").css("display", "block");
  showPersonsInfo(event,personsclick);
}

function showPersonsInfo(event,personsclick){
$("#modal-text--item").append (`
      <p>Street: ${personal[personsclick].address.street}</p>
      <p>Email: ${personal[personsclick].email}</p>
      <p>Username: ${personal[personsclick].username}</p>
      <p>City: ${personal[personsclick].address.city}</p>
      <p>Suite: ${personal[personsclick].address.suite}</p>
      <p>Phone: ${personal[personsclick].phone}</p>
      <p>website: ${personal[personsclick].website}</p>
      <p>Company name: ${personal[personsclick].company.name}</p>
      <p>Company catchPhrase: ${personal[personsclick].company.catchPhrase}</p>
      <p>Company bs: ${personal[personsclick].company.bs}</p>
`);
}

personInfo();