import $ from 'jquery';

var usersList = [
    {
      "_id": "5e9abd32b830663252a1a553",
      "index": 0,
      "guid": "cd2cd136-9664-4757-823e-412a0581626e",
      "isActive": false,
      "balance": "$1,405.94",
      "picture": "./img/lesson-28/1.jpg",
      "age": 35,
      "eyeColor": "blue",
      "name": {
        "first": "Nolan",
        "last": "Dodson"
      },
      "company": "GENMOM",
      "email": "nolan.dodson@genmom.us",
      "phone": "+1 (809) 410-2587",
      "address": "594 Lyme Avenue, Sutton, Minnesota, 6751",
      "about": "Non nostrud exercitation cillum cupidatat officia consequat anim id excepteur non velit anim. Aliquip qui occaecat ad dolore non adipisicing nostrud culpa. Elit est non esse ipsum occaecat nulla cillum enim ex. Aliqua id tempor dolor aute ullamco consequat cupidatat incididunt duis labore officia adipisicing. Pariatur consequat ex reprehenderit irure aliqua consectetur.",
      "registered": "Thursday, July 16, 2015 1:23 PM",
      "friends": [
        {
          "id": 0,
          "name": "Liz Underwood"
        },
        {
          "id": 1,
          "name": "Jennie Lynn"
        },
        {
          "id": 2,
          "name": "Kristine Cochran"
        }
      ],
      "greeting": "Hello, Nolan! You have 10 unread messages.",
      "favoriteFruit": "apple"
    },
    {
      "_id": "5e9abd3265bc71d749924ace",
      "index": 1,
      "guid": "bc7ea661-cebf-4cb6-b13c-70c511ce37f8",
      "isActive": true,
      "balance": "$1,378.85",
      "picture": "./img/lesson-28/2.jpg",
      "age": 25,
      "eyeColor": "brown",
      "name": {
        "first": "Castillo",
        "last": "Berg"
      },
      "company": "QUILTIGEN",
      "email": "castillo.berg@quiltigen.net",
      "phone": "+1 (876) 564-2364",
      "address": "224 Kenilworth Place, Spelter, Arizona, 6558",
      "about": "Sit in duis deserunt reprehenderit eiusmod non minim dolor velit veniam. Duis ut irure esse id amet culpa aliquip amet incididunt ipsum excepteur sint aliquip. Dolore quis Lorem fugiat duis. Tempor commodo ex irure aliqua reprehenderit ipsum. Consequat ipsum nostrud dolor aliqua id nostrud deserunt magna adipisicing deserunt. Pariatur laboris enim occaecat culpa ullamco excepteur sunt ipsum dolore anim amet anim. Enim pariatur aliqua duis Lorem qui et commodo adipisicing.",
      "registered": "Sunday, December 20, 2015 4:36 AM",
      "friends": [
        {
          "id": 0,
          "name": "Oliver Price"
        },
        {
          "id": 1,
          "name": "Ramirez Justice"
        },
        {
          "id": 2,
          "name": "Brooks Wood"
        }
      ],
      "greeting": "Hello, Castillo! You have 8 unread messages.",
      "favoriteFruit": "strawberry"
    },
    {
      "_id": "5e9abd32a899a5f7637128cf",
      "index": 2,
      "guid": "a673fa9b-283d-40a8-a9a7-67fd98b93f3b",
      "isActive": true,
      "balance": "$3,211.36",
      "picture": "./img/lesson-28/3.jpg",
      "age": 40,
      "eyeColor": "green",
      "name": {
        "first": "Agnes",
        "last": "Beck"
      },
      "company": "PARLEYNET",
      "email": "agnes.beck@parleynet.co.uk",
      "phone": "+1 (856) 507-3029",
      "address": "237 Channel Avenue, Warsaw, Delaware, 903",
      "about": "Id excepteur voluptate commodo labore cupidatat et cupidatat ea velit nostrud. Ea nisi non laboris aliquip consequat commodo non anim officia deserunt occaecat labore occaecat. Pariatur nostrud ut culpa anim qui occaecat.",
      "registered": "Thursday, February 12, 2015 6:36 PM",
      "friends": [
        {
          "id": 0,
          "name": "Ford Sosa"
        },
        {
          "id": 1,
          "name": "Gabrielle Heath"
        },
        {
          "id": 2,
          "name": "Bryant Colon"
        }
      ],
      "greeting": "Hello, Agnes! You have 5 unread messages.",
      "favoriteFruit": "apple"
    },
    {
      "_id": "5e9abd32e07ba54a8f034819",
      "index": 3,
      "guid": "fc5c988c-259b-45f4-8923-91ac986a632d",
      "isActive": false,
      "balance": "$2,524.56",
      "picture": "./img/lesson-28/4.jpeg",
      "age": 29,
      "eyeColor": "brown",
      "name": {
        "first": "Carver",
        "last": "Rutledge"
      },
      "company": "WEBIOTIC",
      "email": "carver.rutledge@webiotic.org",
      "phone": "+1 (968) 506-3048",
      "address": "239 Hunterfly Place, Lloyd, Federated States Of Micronesia, 6019",
      "about": "Laborum minim labore cillum deserunt officia cillum esse. Laboris eiusmod velit id non duis ullamco id cupidatat. Laborum velit mollit ad labore consectetur incididunt. In adipisicing exercitation velit in. Velit duis nostrud voluptate culpa commodo eiusmod Lorem anim.",
      "registered": "Tuesday, April 12, 2016 7:48 AM",
      "friends": [
        {
          "id": 0,
          "name": "Wolfe Mccoy"
        },
        {
          "id": 1,
          "name": "Holden Hull"
        },
        {
          "id": 2,
          "name": "Janine Durham"
        }
      ],
      "greeting": "Hello, Carver! You have 7 unread messages.",
      "favoriteFruit": "banana"
    },
    {
      "_id": "5e9abd321b1d9075a80a2e88",
      "index": 4,
      "guid": "f446ff8f-aa63-4a61-ad7a-e02fd17435eb",
      "isActive": false,
      "balance": "$3,775.05",
      "picture": "./img/lesson-28/5.jpg",
      "age": 38,
      "eyeColor": "brown",
      "name": {
        "first": "Ingram",
        "last": "Stevenson"
      },
      "company": "JETSILK",
      "email": "ingram.stevenson@jetsilk.ca",
      "phone": "+1 (921) 576-2744",
      "address": "223 Bath Avenue, Hoehne, Pennsylvania, 4351",
      "about": "Aliquip in aliqua commodo laboris. Eu ex ut incididunt duis voluptate sit adipisicing. Duis cillum aute ullamco consectetur. Laborum sit ad laboris exercitation.",
      "registered": "Thursday, January 16, 2014 9:01 AM",
      "friends": [
        {
          "id": 0,
          "name": "Daniel Bowman"
        },
        {
          "id": 1,
          "name": "Higgins Abbott"
        },
        {
          "id": 2,
          "name": "Willa Gardner"
        }
      ],
      "greeting": "Hello, Ingram! You have 8 unread messages.",
      "favoriteFruit": "apple"
    }
  ]
                                                 
// var usersListA = usersListB.replace(/"_id"/i, '"id"');    // -не получилось заменить _id на id.  
// var usersListB = usersListC.replace($, '');               // -не получилось убрать $ в "balance"
// console.log(usersListC);                                 

function ConstructorPerson(list, personsHolder, filtersHolder, showFullInfo) {
  this.list = list;
  this.personsHolder = personsHolder;
  this.filtersHolder = filtersHolder;
  this.showFullInfo = showFullInfo;

  this.nameSearch = '';
  this.companyList = '';
  this.ageFrom = '';
  this.ageTo = '';
  this.eyeField = '';
  this.fruitFavorit = '';
  this.minMaxSalary = '';

  this.init = function() {
    this.initPersonsList(this.list);
    this.initPersonsFilers();
    this.addEventsListeners();
  }
  this.initPersonsList = function(list) {
    const self = this;
    this.personsHolder.html('');
    list.map(function(elem, index) {
      self.personsHolder.append(`
      <div class="persons__list--item">
        <img src="${elem.picture}">
        <p>${elem.name.first} ${elem.name.last}</p>
       <button class = "show__more" data-id = "${index}">Show full info</button>
      </div>`);
    });
  };
  this.initPersonsFilers = function() {
    let uniqCompany = [];
    for(var i = 0; i < this.list.length; i++) {
      uniqCompany.indexOf(this.list[i].company) == -1 ? uniqCompany.push(this.list[i].company) : null;
    };
    uniqCompany.map(function(elem) {
      $('#companyList').append(`<option value = "${elem}">${elem}</option>`);
    });

    let uniqEyeInField = [];
    for(let i = 0; i < this.list.length; i++) {
        uniqEyeInField.indexOf(this.list[i].eyeColor) == -1 ? uniqEyeInField.push(this.list[i].eyeColor) : null;
    }
    uniqEyeInField.map(function(elem){
        $('#eye__field').append(`
        <div class="${elem}">
           <label for="${elem}" class='v__check'>${elem}</label>
           <input type="checkbox" value ="${elem}">
         </div>
      `);
    });

    let uniqfruitFavorit = [];
    for(let i = 0; i < this.list.length; i++) {
        uniqfruitFavorit.indexOf(this.list[i].favoriteFruit) == -1 ? uniqfruitFavorit.push(this.list[i].favoriteFruit) : null;
    };
    uniqfruitFavorit.map(function(elem){
        $('#favoriteFruit__field').append(`   
        <div class="${elem}">
           <label for="${elem}" class='v__check'>${elem}</label>
           <input type="radio" value ="${elem}" >
        </div>
      `);
    });
  };

  this.addEventsListeners = function() {
    const self = this;
    $('#nameSearch').on('change', function(e) {
      self.nameSearch = e.target.value;
    });

    $('#companyList').on('change', function(e) {
      self.companyList = $(this).children("option:selected").val();
    });

    $('#min__year').on('change', function(e){
      self.ageFrom = e.target.value;
    });

    $('#max__year').on('change', function(e){
      self.ageTo = e.target.value;
    });

    $('#eye__field').on('change', function(e) {       // ---  так работает только по текущему изменению чекбокса
      self.eyeField = e.target.value;
    });
    // $('#eye__field').on('change', function(e) {    // ---  так вообще не работает 
    //     if (e.target.value == true) {
    //     eyeField.push(e.target.value)
    //   } else {
    //     self.eyeField.splice()
    //   } 
    // });

    $('#favoriteFruit__field').on('change', function(e){
      self.fruitFavorit = e.target.value;
    });

    $('#min_max_salary').on('change', function(e){
      self.minMaxSalary = e.target.value;
    });

    $('#submit').on('click', function() {
      self.filtering();
    });

    $('.show__more').each(function(i, element){
      element.onclick = openModal;
    });
    $("#closeMyModal").on("click", function(){   
          $("#myModal").css("display", "none");
          $("#modal-text--item").empty();
    });
    function openModal(event) {
    var personsclick = event.target.dataset.id;
    $("#myModal").css("display", "block");
    showPersonsInfo (event, personsclick);
    };
    function showPersonsInfo (event, personsclick) {
    $("#modal-text--item").append (`
    <p><span>Name:</span> ${self.list[personsclick].name.first} ${self.list[personsclick].name.last}</p>
    <p><span>Id:</span> ${self.list[personsclick]._id}</p>
    <p><span>Index:</span> ${self.list[personsclick].index}</p>
    <p><span>Guid:</span> ${self.list[personsclick].guid}</p>
    <p><span>IsActive:</span> ${self.list[personsclick].isActive}</p>
    <p><span>Balance:</span> ${self.list[personsclick].balance}</p>
    <p><span>EyeColor:</span> ${self.list[personsclick].eyeColor}</p>
    <p><span>Company:</span> ${self.list[personsclick].company}</p>
    <p><span>Email:</span> ${self.list[personsclick].email}</p>
    <p><span>Phone:</span> ${self.list[personsclick].phone}</p>
    <p><span>Address:</span> ${self.list[personsclick].address}</p>
    <p><span>About:</span> ${self.list[personsclick].about}</p>
    <p><span>Registered:</span> ${self.list[personsclick].registered}</p>
    <p><span>Friends:</span>
     ${self.list[personsclick].friends[0].name},
     ${self.list[personsclick].friends[1].name},
     ${self.list[personsclick].friends[2].name}</p>
    <p><span>Greeting:</span> ${self.list[personsclick].greeting}</p>
    <p><span>FavoriteFruit:</span> ${self.list[personsclick].favoriteFruit}</p>
    `)};
    };
    
  this.filtering = function() {
    const self = this;
    var newArr = this.list.filter(function(item) {
      return item.name.first.indexOf(self.nameSearch) > -1 // Как в это поле добавить и поиск по фамилии?
       && (self.companyList === '' || self.companyList == item.company)
       && (self.ageFrom === '' || self.ageFrom <= item.age)
       && (self.ageTo === '' || self.ageTo >= item.age)
       && (self.eyeField === '' || self.eyeField == item.eyeColor)
       && (self.fruitFavorit === '' || self.fruitFavorit == item.favoriteFruit)
    });
    this.initPersonsList(newArr);
  };
};

let personListForm = new ConstructorPerson(usersList, $('.persons__list'), $('.persons__filters'), $('#myModal'));
personListForm.init();


  
 




