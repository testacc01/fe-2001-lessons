/* BURGER MENU */

var modal = document.getElementsByClassName("nav__burger")[0];
var btn1 = document.getElementsByClassName("open__burger")[0];
var btn2 = document.getElementsByClassName("nav__burger--close")[0];


btn1.onclick = function() {
    modal.style.display = "block";
    btn1.style.display = "none";
}

btn2.onclick = function() {
    modal.style.display = "none";
    btn1.style.display = "block";
}

window.onclick = function(event) {
    if (event.target == modal) {
        modal.style.display = "none";
        btn1.style.display = "block";    
    }

}


/* OurServices */

// Version 1 (work)

var elements = document.querySelectorAll(".ourServices__box--item1");
for (var i=1;i<elements.length+1;i++){

    var more = document.getElementsByClassName("ourServices__description--more"+i)[0];
    var back = document.getElementsByClassName("ourServices__description--close"+i)[0];

    more.onclick = function(){
        this.parentElement.parentElement.querySelector('.ourServices__description').style.display = 'block'
        this.parentElement.parentElement.querySelector('.ourServices__description--intro').style.display = 'none'
    }  

    back.onclick = function(){
        this.parentElement.parentElement.querySelector('.ourServices__description').style.display = 'none'
        this.parentElement.parentElement.querySelector('.ourServices__description--intro').style.display = 'block'
    }
   
}


// Version 2 (work)

// var description1 = document.getElementsByClassName("ourServices__description1")[0];
// var intro1 = document.getElementsByClassName("ourServices__description1--intro")[0];
// var more1 = document.getElementsByClassName("ourServices__description--more1")[0];
// var back1 = document.getElementsByClassName("ourServices__description--close1")[0];


// more1.onclick = function() {
//     description1.style.display = "block";
//     intro1.style.display = "none";
// }

// back1.onclick = function() {
//     description1.style.display = "none";
//     intro1.style.display = "block";
// }

// var description2 = document.getElementsByClassName("ourServices__description2")[0];
// var intro2 = document.getElementsByClassName("ourServices__description2--intro")[0];
// var more2 = document.getElementsByClassName("ourServices__description--more2")[0];
// var back2 = document.getElementsByClassName("ourServices__description--close2")[0];


// more2.onclick = function() {
//     description2.style.display = "block";
//     intro2.style.display = "none";
// }

// back2.onclick = function() {
//     description2.style.display = "none";
//     intro2.style.display = "block";
// }

// var description3 = document.getElementsByClassName("ourServices__description3")[0];
// var intro3 = document.getElementsByClassName("ourServices__description3--intro")[0];
// var more3 = document.getElementsByClassName("ourServices__description--more3")[0];
// var back3 = document.getElementsByClassName("ourServices__description--close3")[0];


// more3.onclick = function() {
//     description3.style.display = "block";
//     intro3.style.display = "none";
// }

// back3.onclick = function() {
//     description3.style.display = "none";
//     intro3.style.display = "block";
// }

// var description4 = document.getElementsByClassName("ourServices__description4")[0];
// var intro4 = document.getElementsByClassName("ourServices__description4--intro")[0];
// var more4 = document.getElementsByClassName("ourServices__description--more4")[0];
// var back4 = document.getElementsByClassName("ourServices__description--close4")[0];


// more4.onclick = function() {
//     description4.style.display = "block";
//     intro4.style.display = "none";
// }

// back4.onclick = function() {
//     description4.style.display = "none";
//     intro4.style.display = "block";
// }

// var description5 = document.getElementsByClassName("ourServices__description5")[0];
// var intro5 = document.getElementsByClassName("ourServices__description5--intro")[0];
// var more5 = document.getElementsByClassName("ourServices__description--more5")[0];
// var back5 = document.getElementsByClassName("ourServices__description--close5")[0];


// more5.onclick = function() {
//     description5.style.display = "block";
//     intro5.style.display = "none";
// }

// back5.onclick = function() {
//     description5.style.display = "none";
//     intro5.style.display = "block";
// }

// var description6 = document.getElementsByClassName("ourServices__description6")[0];
// var intro6 = document.getElementsByClassName("ourServices__description6--intro")[0];
// var more6 = document.getElementsByClassName("ourServices__description--more6")[0];
// var back6 = document.getElementsByClassName("ourServices__description--close6")[0];


// more6.onclick = function() {
//     description6.style.display = "block";
//     intro6.style.display = "none";
// }

// back6.onclick = function() {
//     description6.style.display = "none";
//     intro6.style.display = "block";
// }



// LOGO


// Version 1 (work)

// var logo = document.getElementsByClassName("logo")[0];
// var gray = document.getElementsByClassName("logo__gray--head")[0];
// var color = document.getElementsByClassName("logo__color--head")[0];
// var gray1 = document.getElementsByClassName("logo__gray1")[0];
// var color1 = document.getElementsByClassName("logo__color1")[0];
// var gray2 = document.getElementsByClassName("logo__gray2")[0];
// var color2 = document.getElementsByClassName("logo__color2")[0];


// logo.onclick = function() {
//     gray.style.display = "none";
//     color.style.display = "block";
//     gray1.style.display = "none";
//     color1.style.display = "block";
//     gray2.style.display = "none";
//     color2.style.display = "block";
// }


// Version 2 (work)

var elements1 = document.querySelectorAll(".logo__gray");
var elements2 = document.querySelectorAll(".logo__color");
var logo = document.querySelectorAll('.logo')[0]


logo.onmouseover = logo.onmouseout  = function(event){
    if (event.type == 'mouseover') {
        for(var k = 0;k<elements1.length; k++){
            elements1[k].style.display = 'none'
            elements2[k].style.display = 'block'
        }
      }
      if (event.type == 'mouseout') {
        for(var k = 0;k<elements1.length; k++){
            elements1[k].style.display = 'block'
            elements2[k].style.display = 'none'
        }
      }
  };