// Example #1

function salariesSum(arr) {
  let maxSum = 0; 

  for (let i = 0; i < arr.length; i++) {
      maxSum +=  arr[i];
    }
  return maxSum;
}

let salaries = [100, 160, 130];
alert( salariesSum(salaries) );



// Example #2

function copySorted(arr) {
  return arr.slice().sort();
}

let arr = ["HTML", "JavaScript", "CSS"];

let sorted = copySorted(arr);

alert( sorted );
alert( arr );



// Example #3

let vasya = { name: "Вася", age: 25 };
let petya = { name: "Петя", age: 30 };
let masha = { name: "Маша", age: 28 };

let users = [ vasya, petya, masha ];

let names = users.map(item => item.name);

alert( names );