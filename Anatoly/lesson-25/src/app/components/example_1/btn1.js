var myObject = { 
    "name": "Поздравление",
    "month": [
        "Январь",
        "Февраль",
        "Март",
        "Апрель",
        "Май",
        "Июнь",
        "Июль",
        "Август"
    ],
    "random array": [
        5,
        true, {
            year: 2018,
            number: 8
        },
        12,
        25.4
    ],
    "year array": [
        2017,
        2018
    ]
  };
  
  // "С", "праздником", "1", "Апреля", "!"
  
  //alert( myObject.name ); // myObject/"name"/"Поздравление"
  //alert( myObject.month["0"] ); // myObject/"month"/"Январь"
  //alert( myObject["random array"]["0"] ); // myObject/"random array"/"5"
  //alert( myObject["random array"]["2"]["number"] ); // myObject/"random array"/"8"
  //alert( myObject["year array"]["1"] ); // myObject/"year array"/"2018"
  
// Задание 1
// 1. Добавляем в массив myObject фразу (С праздником)
myObject["holiday"] = "С";
myObject["dayoff"] = "праздником";

// 2. Добавляем в массив myObject "1"
myObject["month"][8] = "1";

// 3. Заменяем значение "Апрель" на "Апреля"
myObject["month"][3] = "Апреля";

// 4. Добавляем в массив myObject["year array"] "!"
myObject["year array"][2] = "!";

// Задание 2
var btn1 = document.getElementsByClassName("example_1")[0];

btn1.onclick = function() {
   alert( myObject["holiday"] + " " + myObject["dayoff"] + " " + myObject["month"][0] + " " + myObject["month"][2] + myObject["year array"][2]); 
};

// Задание 3
myObject["random array"].pop();

// Задание 4
myObject.month.sort();

// Задание 5
console.log(myObject);
