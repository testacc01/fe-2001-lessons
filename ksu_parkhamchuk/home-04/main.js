let modal = document.getElementById("mw");
let modalAnimation = document.getElementById("animation");
let input = document.getElementById('text_to_change');
let click = 0;

function saveNewValue() {
   let newValue = input.value;
   input.innerHTML = newValue;
   closeWindow();

}

function showWindow() {
    click++;
    console.log(click);
    if (click%2 ==0){
        modalAnimation.className = "modal_window_another p-4";
    }
    else {
        modalAnimation.className = "modal_window p-4";
    }
    modal.style.display = "block";
}

function closeWindow() {
    modal.style.display = "none";
}

window.onclick = function(event){
    if (event.target == modal) {
        modal.style.display = "none";
      }
}
