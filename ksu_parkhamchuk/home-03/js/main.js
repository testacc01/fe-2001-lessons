let modWindow = document.getElementById("modWindow");
 function showAdditional(t){
    t.querySelector(".additional").style.display = "block";
}

function hideAdditional(t) {
    t.querySelector(".additional").style.display = "none";
}

function showModWindow(t) {
   let name =  t.querySelector('.icon_name').innerHTML;
   console.log(name);
   document.getElementById("nameInput").value = name;
   modWindow.style.display = "block";
}

function closeWindow() {
    modWindow.style.display = "none";
}
