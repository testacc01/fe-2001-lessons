let modalBtn = document.getElementById("modalBtn");
let modal = document.getElementById("modal")
let closeBtn = document.getElementById("closeBtn");
let dropdownMenu = document.getElementById("dropdown_menu");
let dropdown = document.getElementById("dropdown");
let cross = document.getElementById("cross");

modalBtn.onclick = function() {
    modal.style.display = "block";
}

closeBtn.onclick = function() {
    modal.style.display = "none";
}

cross.onclick = function() {
    modal.style.display = "none";
}

window.onclick = function(event){
    if (event.target == modal) {
        modal.style.display = "none";
      }
}

dropdown.onmouseover = function() {
    dropdownMenu.style.display = "block";
}

dropdown.onmouseleave = function() {
    dropdownMenu.style.display = "none";
}